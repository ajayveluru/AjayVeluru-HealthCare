/**
 * 
 */
package com.emids.insurance.quote.model;

/**
 * @author emidstest01
 *
 */
public class Habits {
	private String alcohol;
	private String smoking;
	private String dailyExercise;
	private String drugs;
	/**
	 * @return the alcohol
	 */
	public String getAlcohol() {
		return alcohol;
	}
	/**
	 * @param alcohol the alcohol to set
	 */
	public void setAlcohol(String alcohol) {
		this.alcohol = alcohol;
	}
	/**
	 * @return the smoking
	 */
	public String getSmoking() {
		return smoking;
	}
	/**
	 * @param smoking the smoking to set
	 */
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}
	/**
	 * @return the dailyExercise
	 */
	public String getDailyExercise() {
		return dailyExercise;
	}
	/**
	 * @param dailyExercise the dailyExercise to set
	 */
	public void setDailyExercise(String dailyExercise) {
		this.dailyExercise = dailyExercise;
	}
	/**
	 * @return the drugs
	 */
	public String getDrugs() {
		return drugs;
	}
	/**
	 * @param drugs the drugs to set
	 */
	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}
	
	

}
