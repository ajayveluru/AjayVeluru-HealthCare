/**
 * 
 */
package com.emids.insurance.quote.model;

/**
 * @author emidstest01
 *
 */
public class CurrentHealth {
	private String hyperTension;
	private String bloodPressure;
	private String bloodSugar;
	private String overWeight;
	/**
	 * @return the hyperTension
	 */
	public String getHyperTension() {
		return hyperTension;
	}
	/**
	 * @param hyperTension the hyperTension to set
	 */
	public void setHyperTension(String hyperTension) {
		this.hyperTension = hyperTension;
	}
	/**
	 * @return the bloodPressure
	 */
	public String getBloodPressure() {
		return bloodPressure;
	}
	/**
	 * @param bloodPressure the bloodPressure to set
	 */
	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	/**
	 * @return the bloodSugar
	 */
	public String getBloodSugar() {
		return bloodSugar;
	}
	/**
	 * @param bloodSugar the bloodSugar to set
	 */
	public void setBloodSugar(String bloodSugar) {
		this.bloodSugar = bloodSugar;
	}
	/**
	 * @return the overWeight
	 */
	public String getOverWeight() {
		return overWeight;
	}
	/**
	 * @param overWeight the overWeight to set
	 */
	public void setOverWeight(String overWeight) {
		this.overWeight = overWeight;
	}
	
	
}
