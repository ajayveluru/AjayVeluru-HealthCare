/**
 * 
 */
package com.emids.insurance.quote.service;

import com.emids.insurance.quote.model.CurrentHealth;
import com.emids.insurance.quote.model.Habits;
import com.emids.insurance.quote.model.InsurerDetails;

/**
 * @author emidstest01
 *
 */
public class InsuranceValidation {
	
	private double basePremium = 5000.00;
	private InsuranceValidation insuranceValidation = null;
	
	public InsuranceValidation() {
		//default constructor
	}
	
	public InsuranceValidation(InsuranceValidation insuranceValidation) {
		this.insuranceValidation = insuranceValidation;		
	}
	
	/*
	 * get the base premium which is fixed
	 * */
	public double getBasePremium() {
		return basePremium;
	}
	
	public double calculatePremiumOnAge (InsurerDetails id) {
		int age = 0;
		double premiumOnAge = 0d;
		
		if (id == null) {
			//throw exception message
		} else {
			age = id.getAge();
			
			if (age >= 18 && age <25) {
				premiumOnAge = (this.basePremium+(this.basePremium*10/100));
			} else if (age >= 25 && age <30) {
				premiumOnAge = (this.basePremium+(this.basePremium*20/100));
			} else if (age >= 30 && age <35) {
				premiumOnAge = (this.basePremium+(this.basePremium*30/100));
			} else if (age >= 35 && age <40) {
				premiumOnAge = (this.basePremium+(this.basePremium*40/100));
			} else if (age >= 40) {
				premiumOnAge = (this.basePremium+(this.basePremium*60/100));
			}
		}	
							
		return  premiumOnAge;		
	}
	
	
	public double calculateBasePremiumOnGender (InsurerDetails id) {
		String gender;
		double basePremium = 0d;
		
		if (id == null) {
			//throw exception message
		} else {
			gender = id.getGender();
			
			if (gender.equalsIgnoreCase("Male")) {
				basePremium += basePremium*2/100;
			}
		}	
							
		return  basePremium;		
	}
	
	/*
	 * It calculates the premium based on Precondition Health Issues
	 * */
	public double calculateBasePremiumOnHealthIssues (InsurerDetails id) {
		CurrentHealth currentHealthConditions = null;
		double basePremium = 0d;
		
		if (id == null) {
			//throw exception message
		} else {
			currentHealthConditions = id.getCurrentHealth();
			if (currentHealthConditions != null) {
				if (currentHealthConditions.getBloodPressure().equalsIgnoreCase("Y") ||
						currentHealthConditions.getBloodPressure().equalsIgnoreCase("Y") ||
						currentHealthConditions.getBloodPressure().equalsIgnoreCase("Y") ||
						currentHealthConditions.getBloodPressure().equalsIgnoreCase("Y")) {
					
					basePremium += basePremium*1/100;
				}
			}
		}	
							
		return  basePremium;		
	}

	/*
	 * It calculates the premium based on Habits 
	 * */
	public double calculateBasePremiumOnHabits (InsurerDetails id) {
		Habits habits = null;
		double basePremium = 0d;
		
		if (id == null) {
			//throw exception message
		} else {
			habits = id.getHabits();
			if (habits != null) {
				if (habits.getAlcohol().equalsIgnoreCase("Y") ||						
						habits.getDrugs().equalsIgnoreCase("Y") ||
						habits.getSmoking().equalsIgnoreCase("Y")) {
					
					basePremium += basePremium*3/100;
				} else if (habits.getDailyExercise().equalsIgnoreCase("Y")) {
					basePremium -= basePremium*3/100;
				}
			}
		}	
							
		return  basePremium;		
	}

}
